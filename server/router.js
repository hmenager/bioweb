var program = require('commander');
var color = require('colors');
var _ = require('underscore');
var WebServer = require('./web');
var dataImport = require('./data-import');

var route = function() {
  program.option('-c, --colors', 'Use colors for printing');

  program.command('serve')
    .alias('s')
    .description('Launch HTTP server')
    .action(function(env) {
      WebServer();
    });

  program.command('import')
    .alias('i')
    .description('Import data from import catalog')
    .action(function(env) {
      dataImport();
    });

  program.parse(process.argv);
};

module.exports = route;
