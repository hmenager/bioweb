var MongoClient = require('mongodb').MongoClient;
var getConf = require('./config');

var runQuery = function(queryFunction) {
  var connString = 'mongodb://' + getConf('db.host', 'localhost') + ':' + getConf('db.port', '27017') + '/' + getConf('db.name', 'bioweb');
  var serverOptions = getConf('db.serverOptions',{});
  MongoClient.connect(connString, {server: serverOptions}, function(err, db) {
    queryFunction(err, db);      
  });
};

module.exports = {
  'run': runQuery
};
