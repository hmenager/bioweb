var mongoose = require('mongoose');
var _ = require('underscore');
var exports = {};

var CatalogSchema = mongoose.Schema({
  "_id": String,
  "author": String,
  "bank_format": Array,
  "bank_type": Array,
  "categories": String,
  "default": String,
  "depends": String,
  "description": String,
  "doc": String,
  "history": String,
  "home": String,
  "library": String,
  "name": String,
  "package": String,
  "packages": Array,
  "private": String,
  "program": String,
  "publication_date": String,
  "references": String,
  "removal_date": String,
  "source": String,
  "topics": String,
  "type": String,
  "url": String,
  "version": String
}, {
  collection: "catalog"
});

var Catalog = mongoose.model('Catalog', CatalogSchema);

module.exports = {
  catalog: Catalog
};