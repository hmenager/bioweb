var express = require('express');
var serveStatic = require('serve-static');
var _ = require('underscore');

var getConf = require('./config');
var getSiteMap = require('./sitemap');
var getNewsFeed = require('./news-feed');

var run = require('./mongo-helper').run;

var runMongo = function(res, queryFunction){
  run(function(err, db, callback){
    if(err){
      res.sendStatus(503);
      console.log('Cannot connect to the database.');
    }else{
      queryFunction(db, db.close.bind(db));
    }
  });
}

var router = function(app) {

  var toJSON = function(res, cb, count) {
    return function(err, items) {
      if (err){
        res.sendStatus(500);
        console.error(err);
      }else{
        if(count===true){
          res.json(items.length);
        }else{
          res.json(items);
        }
      }
      cb();
    };
  };

  var toFeed = function(res, cb, accept) {
    return function(err, items) {
      if (err){
        res.status(500).send(err);
      }else{
        getNewsFeed(accept)(items, res);
      }
      cb();
    };
  };

  /* list all the EDAM terms, with optional text, id, and/or subset filter(s) */
  var listEdamTerms = function(req, res, next) {
    var filter = {};
    // project only relevant fields
    var project = {'rdfs:label':1, 'oboInOwl:hasDefinition':1, '@type':1, 'owl:deprecated': 1};
    var count = false;
    if(req.query.count==='true'){
      count = true;
    }
    if (req.params.subset) {
      // we cannot use subsets because deprecated terms are in a separate subset :(
      filter._id = /^http:\/\/edamontology.org\/topic_/;
        //filter['oboInOwl:inSubset'] = {
        //    '@id': 'edam:' + req.params.subset
        //};
    }
    if (req.query.termid) {
      var ids = typeof req.query.termid == 'string' ? [req.query.termid] : req.query.termid;
      //ids = _.map(ids,function(id){return 'http://edamontology.org/'+id;});
      filter._id = {
        '$in': ids
      };
    }
    runMongo(res, function(db, close) {
      var edam = db.collection('edam');
      if (req.query.search) {
        filter.$text = {
          '$search': req.query.search
        };
        project.score = {
          '$meta': "textScore"
        };
        edam.find(filter, project).sort({
          'score': {
            '$meta': "textScore"
          }
        }).toArray(toJSON(res, close, count));
      } else {
        edam.find(filter,project).toArray(toJSON(res, close, count));
      }
    });
  };
  app.get('/api/edam/subsets/:subset', listEdamTerms);
  app.get('/api/edam', listEdamTerms);

  /* get an EDAM term by ID */
  app.get('/api/edam/:id', function(req, res, next) {
    runMongo(res, function(db, close) {
      var edam = db.collection('edam');
      edam.findOne({
        '_id': req.params.id
      }, toJSON(res, close));
    });
  });

  /* list the latest news */
  app.get('/api/news', function(req, res, next) {
    var respType = req.accepts('application/rss+xml', 'application/json', 'application/atom+xml');
    var callback;
    var format;
    switch(respType){
        case false:
  	    res.status(406).send('Not Acceptable');
	    return;
        case 'application/json':
            callback = toJSON;
            break;
        case 'application/atom+xml':
            callback = toFeed;
            break;
        case 'application/rss+xml':
            callback = toFeed;
            break;
    }
    runMongo(res, function(db, close) {
      var news = db.collection('news');
      var limit = req.query.limit;
      var query = [{ $sort : { "date" : -1}}];
      if(limit!==undefined){
        query.push({ $limit : parseInt(limit)});
      }
      news.aggregate(query).toArray(callback(res, close, respType));
    });
  });

  app.get('/sitemap.xml', getSiteMap);

  app.get('/analytics', function(req, res, next) {
    var gaCode = getConf("web.gacode", "UA-XXXXXXXX-X");
    var gaJsText = `(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', '`+ gaCode +`', 'auto');
      ga('send', 'pageview');`
    res.send(gaJsText);
  });

  /* list topic IDs and corresponding package count */
  app.get('/api/packageTopics', function(req, res, next){
    var count = false;
    if(req.query.count==='true'){
      count = true;
    }
    runMongo(res, function(db, close) {
      var packagesIndex = db.collection('packagesIndex');
      var edam = db.collection('edam');
      var aggregatePipe = [
        {$match:{_id: /^http:\/\/edamontology.org\/topic_/}},
        {$lookup:{from: 'packagesIndex', localField: '_id', foreignField: 'topics', as:'defs'}},
        {$project:{'_id':1, 'count':{$size:'$defs'}}},
        {$match:{count:{$gt:0}}}
      ]
      if (req.query.search) {
        aggregatePipe.unshift({$match: {
          '$text':{'$search': req.query.search}
        }});
      }
      edam.aggregate(aggregatePipe).toArray(toJSON(res,close, count));
    });
  });


  /* list/get package or databanks related entries (package, packageVersions, programs,
  mobyles, galaxys, data) */
  app.get('/api/:collection/:id?', function(req, res, next) {
    runMongo(res, function(db, close) {
      var authorizedCollections = ['packages', 'packageVersions', 'programs'
                                  , 'mobyles', 'galaxys', 'data'];
      if(authorizedCollections.indexOf(req.params.collection)==-1){
        res.sendStatus(404);
        close();
        return;
      }
      var collection = db.collection(req.params.collection);
      var filter = {};
      if(req.params.id){
        filter._id = req.params.id;
      }
      var count = false;
      if(req.query.count==='true'){
        count = true;
      }
      var limit = 0;
      if (req.query.limit) {
        limit = parseInt(req.query.limit);
      }
      if (req.query.collection){
        switch (req.params.collection) {
          case 'packages':
            filter['collections'] = req.query.collection;
            break;
          case 'packageVersions':
            filter['package.collections'] = req.query.collection;
            break;
          case 'programs':
            filter['packageVersion.package.collections'] = req.query.collection;
            break;
          default:
            res.status(422).send("Unprocessable Entity");
            close();
            return;
          }
      }
      if (req.query.web=='true'){
        switch (req.params.collection) {
          case 'packages':
            filter['$or'] = [{'packageVersions.programs.mobyles.0' : {'$exists': true}},
                             {'packageVersions.programs.galaxys.0' : {'$exists': true}}];
            break;
          case 'packageVersions':
            filter['$or'] = [{'programs.mobyles.0' : {'$exists': true}},
                             {'programs.galaxys.0' : {'$exists': true}}];
            break;
          case 'programs':
            filter['$or'] = [{'mobyles.0' : {'$exists': true}},
                             {'galaxys.0' : {'$exists': true}}];
            break;
          default:
            res.status(422).send("Unprocessable Entity");
            close();
          }
      }
      if (req.query.topicId) {
        if(req.params.collection!='packages'){
          res.status(422).send("Unprocessable Entity");
        }
        filter.topics = {
          '$in': [req.query.topicId, 'http://edamontology.org/' + req.query.topicId]
        };
      }
      if (req.query.search) {
        filter.$or = [
           {
             name: new RegExp(req.query.search,'i')
           },
           {
             authors: new RegExp(req.query.search,'i')
           },
           {
             'packageVersions.programs.name': new RegExp(req.query.search,'i')
           },
           {
             'references.citation': new RegExp(req.query.search,'i')
           },
        ];
        collection.find(filter, {
          'score': {
            '$meta': "textScore"
          }
        }).sort({
          'score': {
            '$meta': "textScore"
          }
        }).toArray(toJSON(res, close, count));
      } else {
        collection.find(filter, {
          'limit': limit
        }).toArray(
          function(err, items){
            if(req.params.id){
              toJSON(res, close, count)(err, items[0]);
            }else{
              toJSON(res, close, count)(err, items);
            }
          }
        );
      }
    });
  });

};

var WebServer = function(contacts) {
  var app = express();
  var staticMw = serveStatic(__dirname + '/../client/app', {
    'index': ['index.html']
  });
  app.use(staticMw);
  app.use('/welcome',staticMw);
  app.use('/about',staticMw);
  app.use('/topics',staticMw);
  app.use('/data',staticMw);
  app.use('/web',staticMw);
  app.use('/pasteur',staticMw);
  app.use('/packages',staticMw);
  app.use('/packages/:package_id',staticMw);
  app.use('/packages/topics/:topic_id',staticMw);
  app.use('/news',staticMw);
  var webPort = getConf("web.port", 8080);
  var webHost = getConf("web.host", null);
  server = app.listen(webPort, webHost);
  router(app);
  server.on('listening', function(){
    var address = server.address();
    console.log('Server listening to', address.address+':'+address.port);
  });
};

var launch = function() {
  WebServer();
};

module.exports = launch;
