var _ = require('underscore');
var color = require('colors');
var async = require('async');

var runMongo = require('./mongo-helper').run;

var dataImport = function(){

  runMongo(function(db, close) {
    var catalog = db.collection('catalog');

    var runAggregate = function(collectionName, pipeline, description){
      // function to run an aggregation
      return function(callback){
          console.log('starting: '.gray, description.gray);
          var collection = db.collection(collectionName);
          collection.aggregate(pipeline,{}, function(err, res){
            if(err){
              throw 'Error while processing '.red, description.red, err;
            }
            console.log('finished: '.green, description.green);
            callback();
          })
      }
    };

    var runCreateIndex = function(collectionName, indexDefinition, options, description){
      // function to run an index
      return function(callback){
          console.log('starting: '.gray, description.gray);
          options = options || {};
          options.name = description;
          var collection = db.collection(collectionName);
          collection.dropIndex(description, {}, function(res){
              collection.createIndex(indexDefinition, {'name':description}, function(err, res){
                if(err){
                  throw 'Error while processing '.red, description.red, err;
                }
                console.log('finished: '.green, description.green);
                callback();
              });
            }
          );
      };
    };

    var processTopics = function(callback){
      // function to generate package topics from gensoft categories
      // initial package entry contains a key "categories" containing a list like [ "EDAM-0000492", "EDAM-0000159" ]
      // processed package entry contains a key "topics" containing a list like [ "http://edamontology.org/topic_0159" ]
      console.log('starting: gensoft topics processing'.gray);
      db.collection('edam').find({}, {
        '_id': 1
      }).toArray(function(err, items){
        var topics = _.pluck(items,'_id');
        db.collection('catalog').find({'type':'package'}).forEach(function(item){
          item.topics = [];
          _.each(item.categories, function(category) {
            var id = 'http://edamontology.org/topic_' + category.slice(-4);
            if (_.indexOf(topics, id) > -1) {
              item.topics.push(id);
            }
          });
          db.collection('catalog').save(item);
        }, function(err){
          console.log('finished: gensoft topics processing'.green);
          callback();
        })
      });
    };

    var processVersions = function(callback){
      // function to find versions in gensoft packages history
      console.log('starting: gensoft history versions processing'.gray);
      db.collection('catalog').find({'type':'package'}).forEach(function(item){
        _.map(item.history, function(historyItem){
           var result = new RegExp("version ([^ ]*)",'g').exec(historyItem.message);
           if(result){
               historyItem.version = result[1];
           }
        });
        db.collection('catalog').save(item);
      }, function(err){
        console.log('finished: gensoft history versions'.green);
        callback();
      });
    };

    var processBioMajDates = function(callback){
      // function to generate date objects for biomaj entries from the string
      console.log('starting: biomaj dates processing'.gray);
      db.collection('catalog').find({'type':'bank'}).forEach(function(doc) {
        doc.publication_date = new Date(doc.publication_date);
        doc.removal_date = new Date(doc.removal_date);
        db.collection('catalog').save(doc);
      }, function(err){
        console.log('finished: biomaj dates processing'.green);
        callback();
      });
    };

    var lookupFromArray = function(localCollection, localField, foreignCollection, foreignField, description){
      return function(callback){
        // function to make a $lookup equivalent from local values embedded in an array
        console.log('starting: '.gray, description.gray);
        var processDocument = function(doc, callback) {
          async.map(doc[localField], function(localFieldItem, callback){
            var filter = {};
            filter[foreignField] = localFieldItem;
            db.collection(foreignCollection).findOne(filter, function(err, doc){
              callback(err, doc);
            });
          }, function(err, results){
            if(err){
              throw err;
            }
            doc[localField] = results;
            db.collection(localCollection).save(doc, null, function(err, res){
              callback();
            });
          });
        };
        var processDocumentQ = async.queue(processDocument, Infinity);
        var cursor = db.collection(localCollection).find()
        cursor.each(function(err, doc){
          if (err) throw err;
          if (doc) processDocumentQ.push(doc); // dispatching doc to async.queue
        });
        processDocumentQ.drain = function() {
          if (cursor.isClosed()) {
            console.log('finished: '.green, description.green);
            callback();
          }
        };
      };
    };

    var createNews = function(callback){
      console.log('starting: news processing'.gray);
      db.dropCollection('news');
      var newsP = db.createCollection('news');
      newsP.then(function(news){
        async.series([
          function(callback){
            //inserting packages in news collection
            db.collection('packages')
                .aggregate([ { $unwind : "$history"}
                           , {$project: {"name":1
                                       , "type":1
                                       , "date":"$history.date"
                                       , "message":"$history.message"
                                       , "operation":"$history.operation"
                                       , version:"$history.version"
                                       , "_id":0}}]).forEach(function(doc){news.insert(doc);}
                           , function(err){callback();});
          },
          function(callback){
            //inserting databanks in news collection
            db.collection('data')
                .aggregate([ {$project: {"name":1
                                        , "type":1
                                        , "date":"$publication_date"
                                        , "message":{$literal: "databank update"}
                                        , "operation":{$literal: "databank update"}
                                        , "_id":0}}]).forEach(function(doc){news.insert(doc);}
                           , function(err){callback();});
          },
          function(callback){
            //inserting galaxy in news collection
            db.collection('catalog')
                .aggregate([{$match:{'type':'galaxy'}}
                           ,{$project: {"name":1
                                       , "type":1
                                       , "date":"$publication_date"
                                       , "message":{$literal: "New galaxy interface"}
                                       , "operation":{$literal: "galaxy update"}
                                       , "_id":0}}]).forEach(function(doc){news.insert(doc);}
                           , function(err){callback();});
          }], function(err){
            console.log('finished: news processing'.green);
            callback();
        });
      });
    };

    async.series([
      processTopics,
      processBioMajDates,
      processVersions,
      runAggregate('catalog', [{$match:{'type':'bank', status:'online'}}
                              , {$out:'data'}]
                              , 'create banks collection'),
      runAggregate('catalog', [{$match:{'type':'package'}}
                              , {$out:'_packages'}]
                              , 'create packages collection'),
      runAggregate('catalog', [{$match:{'type':'packageVersion'}}
                              , {$out:'_packageVersions'}]
                              , 'create packageVersions collection'),
      runAggregate('catalog', [{$match:{'type':'program'}}
                              , {$out:'_programs'}]
                              , 'create programs collection'),
      runAggregate('catalog', [{$match:{'type':'mobyle'}}
                              , {$out:'_mobyles'}]
                              , 'create mobyles collection'),
      runAggregate('catalog', [{$match:{'type':'galaxy'}}
                              , {$out:'_galaxys'}]
                              , 'create galaxys collection'),
      runAggregate('_mobyles', [{$unwind:'$programs'}
                              , {$project:{'_id':0, 'program':'$programs', 'mobyle':'$_id'}}
                              , {$lookup:{from:'_mobyles', localField:'mobyle', foreignField:'_id', as:'mobyle'}}
                              , {$unwind:'$mobyle'}
                              , {$out:'_mobylePrograms'}]
                              , 'create mobylePrograms temporary collection'),
      runAggregate('_galaxys', [{$unwind:'$programs'}
                              , {$project:{'_id':0, 'program':'$programs', 'galaxy':'$_id'}}
                              , {$lookup:{from:'_galaxys', localField:'galaxy', foreignField:'_id', as:'galaxy'}}
                              , {$unwind:'$galaxy'}
                              , {$out:'_galaxyPrograms'}]
                              , 'create galaxyPrograms temporary collection'),
      runAggregate('_mobyles',[{$out:'mobyles'}], 'create mobyles collection'),
      runAggregate('_galaxys',[{$out:'galaxys'}], 'create galaxys collection'),
      runAggregate('_programs', [{$lookup:{from:'_galaxyPrograms', localField:'_id', foreignField:'program', as:'galaxys'}}
                              , {$lookup:{from:'_mobylePrograms', localField:'_id', foreignField:'program', as:'mobyles'}}
                              , {$project:{'galaxys':'$galaxys.galaxy'
                                           ,'mobyles':'$mobyles.mobyle'
                                           ,'name':1
                                           ,'packageVersion':1
                                           ,'doc':1
                                           ,'description':1}}
                              , {$out:'programs'}]
                              , 'add children to programs collection'),
      runAggregate('_packageVersions', [{$lookup:{from:'programs', localField:'_id', foreignField:'packageVersion', as:'programs'}}
                              , {$out:'packageVersions'}]
                              , 'add children to packageVersions collection'),
      runAggregate('_packages', [{$lookup:{from:'packageVersions', localField:'_id', foreignField:'package', as:'packageVersions'}}
                              , {$out:'packages'}]
                              , 'add children to packages collection'),
      runAggregate('packageVersions', [{$lookup:{from:'_packages', localField:'package', foreignField:'_id', as:'package'}}
                              , {$unwind:'$package'}
                              , {$out:'packageVersions'}]
                              , 'add parents to packageVersions collection'),
      runAggregate('programs', [{$lookup:{from:'_packageVersions', localField:'packageVersion', foreignField:'_id', as:'packageVersion'}}
                              , {$unwind:'$packageVersion'}
                              , {$lookup:{from:'_packages', localField:'packageVersion.package', foreignField:'_id', as:'packageVersion.package'}}
                              , {$unwind:'$packageVersion.package'}
                              , {$out:'programs'}]
                              , 'add parents to programs collection'),
      lookupFromArray('data', 'packageVersions', 'packageVersions', '_id', 'link databanks to packages they are indexed for'),
      runCreateIndex('packages',{'name':'text', 'references.citation':'text', 'authors':'text', 'packageVersions.programs.name':'text'}, {'weights':{'name':20, 'packageVersions.programs.name':8, 'description':5}}, 'packages text index'),
      runCreateIndex('packageVersions',{'package.name':'text', 'package.references.citation':'text', 'package.authors':'text', 'programs.name':'text'}, {'weights':{'package.name':20, 'programs.name':8, 'package.description':5}}, 'packageVersions text index'),
      runCreateIndex('programs',{'name':'text', 'packageVersion.package.name':'text', 'packageVersion.package.references.citation':'text', 'packageVersion.package.authors':'text', 'packageVersion.programs.name':'text'}, {'weights':{'name':20, 'packageVersion.package.name':20, 'packageVersion.programs.name':8, 'packageVersion.package.description':5}}, 'programs text index'),
      runCreateIndex('mobyles',{'name':'text', 'description':'text', 'program.name':'text', 'program.packageVersion.package.name':'text', 'program.packageVersion.package.references.citation':'text', 'program.packageVersion.package.authors':'text', 'program.packageVersion.programs.name':'text'}, {'weights':{'name':30, 'description':20, 'program.name':20, 'program.packageVersion.package.name':20, 'program.packageVersion.programs.name':8, 'program.packageVersion.package.description':5}}, 'programs text index'),
      runCreateIndex('galaxys',{'name':'text', 'description':'text', 'program.name':'text', 'program.packageVersion.package.name':'text', 'program.packageVersion.package.references.citation':'text', 'program.packageVersion.package.authors':'text', 'program.packageVersion.programs.name':'text'}, {'weights':{'name':30, 'description':20, 'program.name':20, 'program.packageVersion.package.name':20, 'program.packageVersion.programs.name':8, 'program.packageVersion.package.description':5}}, 'programs text index'),
      runCreateIndex('data',{'name':'text', 'description':'text'}, {'weights':{'name':20, 'description':5}}, 'data text index'),
      runCreateIndex('programs', {'packageVersion._id':1}, null, 'programs index on packageVersion'),
      runCreateIndex('packageVersions', {'package._id':1}, null, 'packageVersions index on package'),
      createNews
    ], function(err){
        console.log('done.'.bold.green);
        close();
    });
  });
};

module.exports = dataImport;
