var Feed = require('feed');
var assert = require('assert');
var _ = require('underscore');
var getConf = require('./config');

var getNewsFeed = function(accept){
  return function(items, res) {
    var hostname = getConf("sitemap.baseUrl","http://localhost");
    var feed = new Feed({
      title:          'Bioweb news',
      description:    'Latest news from Bioweb packages, banks, web interfaces, etc.',
      link:           hostname,
    });
    for(var key in items){
      try{
        var item = items[key];
        var feedItem = {
          'title': item.name + ' ' + item.version + ' ' + item.type + ' ' + item.operation,
          'description': item.message,
          'date': item.date
        };
        switch(item.type){
          case 'package':
            feedItem.link = hostname + '/#/packages/' + 'pack@' + item.name + '@' + item.version;
            break;
          default:
            feedItem.link = hostname;
        }
        feed.addItem(feedItem);
      }catch(e){
        console.err("error adding news item:", feedItem);
      }
    }
    switch(accept){
      case 'application/atom+xml':
        format = 'atom-1.0';
        res.set({'Content-Type':accept});
        break;
      case 'application/rss+xml':
        format = 'rss-2.0';
        res.set({'Content-Type':accept});
        break;
    }
    res.send(feed.render(format));
  };
};

module.exports = getNewsFeed;
