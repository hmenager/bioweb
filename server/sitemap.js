var sm = require('sitemap');
var assert = require('assert');
var _ = require('underscore');

var runMongo = require('./mongo-helper').run;
var getConf = require('./config');

var getSiteMap = function(req, res) {
    var urls = [
      { url: '/welcome'},
      { url: '/about'},
      { url: '/topics'},
      { url: '/data'},
      { url: '/packages'},
      { url: '/news'},
    ];
    runMongo(function(db, close) {
      var packagesIndex = db.collection('packagesIndex');
      var cursor = packagesIndex.find({},{'versions._id':1});
      cursor.each(function(err, doc) {
         assert.equal(err, null);
         if (doc !== null) {
            _.each(doc.versions,function(v){
              urls.push('/packages/'+v._id);
            });
         } else {
           var hostname = getConf("sitemap.baseUrl","http://"+req.get('host'));
           sitemap = sm.createSitemap ({
                 hostname: hostname,
                 cacheTime: 600000,        // 600 sec - cache purge period
                 urls: urls
               });
           sitemap.toXML( function (err, xml) {
               if (err) {
                 console.error(err);
                 return res.status(500).end();
               }
               res.header('Content-Type', 'application/xml');
               res.send( xml );
           });
           close();
         }
      });
    });
};

module.exports = getSiteMap;
