#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging
import argparse
import json

from rdflib import Graph, plugin
from rdflib.serializer import Serializer
from rdflib.extras.infixowl import AllProperties

from pymongo import MongoClient, TEXT

def import_ontology(file, db):
    g = Graph().parse(args.file)
    context = {}

    for ns in g.namespaces():
        if str(ns[0])!='':
            context[str(ns[0])] = str(ns[1])

    for p in AllProperties(g):
        if p.qname!='':
            context[p.qname.lstrip(':')] = str(p.identifier)

    context["next_id"] = "http://edamontology.org/next_id"

    edam_json = json.loads(g.serialize(format='json-ld', context=context, indent=4))
    json.dump(edam_json, open('/tmp/EDAM_1.9.jsonld','w'),sort_keys=True,
              indent=4, separators=(',', ': '))

    edam = db.edam
    edam.remove({}, multi=True)
    for doc in edam_json['@graph']:
        doc['_id'] = doc['@id']
        json.dumps(doc)
        edam.insert(doc)
    #create text index
    edam.drop_indexes()
    edam.create_index([("rdfs:label", TEXT), 
                       ("oboInOwl:hasDefinition", TEXT),
                       ("oboInOwl:hasExactSynonym", TEXT),
                       ("oboInOwl:hasNarrowSynonym", TEXT),
                       ("oboInOwl:hasBroadSynonym", TEXT),
                       ("oboInOwl:hasRelatedSynonym", TEXT),
                       ("rdfs:comment", TEXT)
                       ], name="edam_fulltext")

def enrich_with_subclasses(superclass, db):
    subclasses = []
    sc_query = None if superclass['@id'] is None else {'@id':superclass['@id']}
    for term in db.edam.find({'rdfs:subClassOf': sc_query}):
        term = {'@id': term['@id'], 'rdfs:label': term['rdfs:label']}
        term = enrich_with_subclasses(term, db)
        subclasses.append(term)
    if len(subclasses)>0:
        superclass['subclasses'] = subclasses
    return superclass

def generate_hierarchies(db):
    hierarchies = db.hierarchies
    hierarchies.remove({}, multi=True)
    root_ids = [ 'format_1915',
                 'data_0006',
                 'topic_0003',
                 'operation_0004']

    for root_id in root_ids:
        root_id = 'http://edamontology.org/' + root_id
        root = db.edam.find_one({'@id': root_id})
        hierarchies.insert({'_id': root_id, 'tree': enrich_with_subclasses(root, db)})

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                 description='EDAM file import tool')
    parser.add_argument('file',
                        help="path to the EDAM file")
    args = parser.parse_args()

    client = MongoClient()
    db = client.bioweb
    import_ontology(args.file, db)
    generate_hierarchies(db)
