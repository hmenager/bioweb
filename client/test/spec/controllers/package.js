'use strict';

describe('Controller: PackagectrlCtrl', function () {

  // load the controller's module
  beforeEach(module('biowebEvolApp'));

  var PackagectrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PackagectrlCtrl = $controller('PackagectrlCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
