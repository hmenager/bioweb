'use strict';

describe('Controller: DatabanksCtrl', function () {

  // load the controller's module
  beforeEach(module('biowebEvolApp'));

  var DatabanksCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DatabanksCtrl = $controller('DatabanksCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
