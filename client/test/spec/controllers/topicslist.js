'use strict';

describe('Controller: TopicslistCtrl', function () {

  // load the controller's module
  beforeEach(module('biowebEvolApp'));

  var TopicslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TopicslistCtrl = $controller('TopicslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
