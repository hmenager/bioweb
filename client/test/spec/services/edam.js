'use strict';

describe('Service: Edam', function () {

  // load the service's module
  beforeEach(module('biowebEvolApp'));

  // instantiate service
  var Edam;
  beforeEach(inject(function (_Edam_) {
    Edam = _Edam_;
  }));

  it('should do something', function () {
    expect(!!Edam).toBe(true);
  });

});
