'use strict';

describe('Directive: packageLink', function () {

  // load the directive's module
  beforeEach(module('biowebEvolApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<package-link></package-link>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the packageLink directive');
  }));
});
