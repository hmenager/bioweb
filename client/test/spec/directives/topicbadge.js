'use strict';

describe('Directive: TopicBadge', function () {

  // load the directive's module
  beforeEach(module('biowebEvolApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-topic-badge></-topic-badge>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the TopicBadge directive');
  }));
});
