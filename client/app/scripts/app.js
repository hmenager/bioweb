'use strict';

angular.module('biowebEvolApp', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute',
        'ngAnimate',
        'angular-jqcloud',
        'infinite-scroll',
        'angularytics'
    ])
    .filter('highlight', function($sce) {
      // hightlight a given string in a searched text using <strong> tag
      return function(text, phrase) {
        if (phrase) text = text.replace(new RegExp('('+phrase+')', 'gi'),
          '<strong>$1</strong>')
          return $sce.trustAsHtml(text);
        }
      })
    .config(function($routeProvider, $locationProvider, AngularyticsProvider, $httpProvider) {
        $httpProvider.interceptors.push(function($rootScope) {
          return {
            'responseError': function (rejection) {
              if(rejection.status===503){
                $rootScope.unavailable = "This website is currently unavailable";
              }
            }
          }
        });
        AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
        $routeProvider
            .when('/welcome', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                reloadOnSearch: false
            })
            .when('/about', {
                templateUrl: 'views/about.html'
            })
            .when('/news', {
                templateUrl: 'views/news.html',
                controller: 'NewsListCtrl',
                reloadOnSearch: false
            })
            .when('/packages', {
                templateUrl: 'views/packages.html',
                controller: 'PackagesListCtrl',
                resolve: {
                    'webOnly': function() {
                        return false;
                    },
                    'collection': function() {
                        return null;
                    }
                },
                reloadOnSearch: false
            })
            .when('/web', {
                templateUrl: 'views/packages.html',
                controller: 'PackagesListCtrl',
                resolve: {
                    'webOnly': function() {
                        return true;
                    },
                    'collection': function() {
                        return null;
                    }
                },
                reloadOnSearch: false
            })
            .when('/pasteur', {
                templateUrl: 'views/packages.html',
                controller: 'PackagesListCtrl',
                resolve: {
                    'webOnly': function() {
                        return false;
                    },
                    'collection': function() {
                        return 'pasteur';
                    }
                },
                reloadOnSearch: false
            })
            .when('/packages/topics/:topicId', {
                templateUrl: 'views/packages.html',
                controller: 'PackagesListCtrl',
                resolve: {
                    'webOnly': function() {
                        return false;
                    },
                    'collection': function() {
                        return null;
                    }
                },
                reloadOnSearch: false
            })
            .when('/packages/:packageId', {
                templateUrl: 'views/package.html',
                controller: 'PackageCtrl',
                resolve: {
                  'pack': function(Catalog, $route, $q){
                    var pack = $q.defer();
                    var packId = $route.current.params.packageId;
                    Catalog.packageVersions(packId).promise.then(function(result) {
                      pack.resolve(result);
                      pack.reject("package " + packId + " not found");
                    });
                    return pack.promise;
                  }
                }
            })
            .when('/data', {
                templateUrl: 'views/databanks.html',
                controller: 'DatabanksCtrl',
                reloadOnSearch: false
            })
            .when('/topics', {
                templateUrl: 'views/topicslist.html',
                controller: 'TopicsListCtrl',
                resolve: {
                    'webOnly': function() {
                        return false;
                    }
                },
                reloadOnSearch: false
            })
            .otherwise({
                redirectTo: '/welcome'
            });
            $locationProvider.html5Mode(true);
            $locationProvider.hashPrefix('!');
    }).run(function(Angularytics, $rootScope, Catalog, $routeParams) {
      Angularytics.init();
      $rootScope._ = _;
      // $rootScope.sections quick doc:
      // item keys are 'name' for the key
      //               'label' for the human readable title
      //               'subtitle' for the subtitle
      //               'loader' for the loading function
      //               'icon' for the icon class
      $rootScope.sections = [
       {'name': 'topics',
         'label': 'Topics',
         'subtitle': 'Alignment, Structure, etc.',
         'loader': function(){
           return Catalog.packageTopics($routeParams.search, true);
         },
         'icon':'fa-question-circle'
       },
       {'name': 'data',
         'label': 'Databanks',
         'subtitle': 'Banks and Genomes',
         'loader': function(){
           return Catalog.banks($routeParams.search, true);
         },
         'icon':'fa-database'
       },
       {'name': 'packages',
         'label': 'Tools and packages',
         'subtitle': 'Available software packages',
         'loader': function(){
           return Catalog.searchPackages($routeParams.search, $routeParams.topicId, false, true);
         },
         'icon':'fa-wrench'
       },
       {'name': 'web',
         'label': 'Web interfaces',
         'subtitle': 'Mobyle, Galaxy',
         'loader': function(){
           return Catalog.searchPackages($routeParams.search, $routeParams.topicId, true, true);
         },
         'icon':'fa-desktop'
       },
       {'name': 'pasteur',
         'label': 'Pasteur software',
         'subtitle': 'Software from the Institut Pasteur',
         'loader': function(){
           return Catalog.searchPackages($routeParams.search, $routeParams.topicId, true, true, 'pasteur');
         },
         'icon':'fa-compass'
       },
      ];
    });
