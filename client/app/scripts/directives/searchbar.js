'use strict';

angular.module('biowebEvolApp')
    .directive('searchBar', ['$location', '$q', '$routeParams',
        function($location, $q, $routeParams) {
            return {
                templateUrl: '/scripts/directives/searchbar.html',
                scope: {
                  sources: '='
                },
                restrict: 'E',
                link: function(scope) {
                  scope.query = $routeParams.search;
                  scope.search = function(keyEvent) {
                    if (keyEvent.which === 13){
                      $routeParams.search = scope.query;
                    }
                  }
                  scope.go = function() {
                    //test if current value is a selected element in the list
                    var selectedOption = $('#searchbar-options option[value="' + scope.query + '"]');
                      if(selectedOption.length>0){
                        var url = selectedOption.attr('data-url');
                        if (url){
                          $location.path(url);
                        }
                      }
                    };
                }
            };
        }
    ]);
