'use strict';

angular.module('biowebEvolApp')
    .directive('packageLink', ['Catalog',
        function(Catalog) {
            var getPackId = function(pack, packVersion){
              var url = 'pack@'+pack.name;
              if(packVersion){
                url += '@' + packVersion.version;
              }
              return url;
            }
            return {
                templateUrl: '/scripts/directives/packagelink.html',
                scope: {
                    packageVersionId: '@',
                    packageVersion: '=',
                    package: '=',
                    withName: '@',
                    noDefaultTag: '@'
                },
                restrict: 'E',
                link: function(scope) {
                    if (scope.packageVersionId) {
                        Catalog.packageVersions(scope.packageVersionId).promise.then(function(packageVersion) {
                          scope.packageVersion = packageVersion;
                          scope.package = packageVersion.package;
                          scope.packId = getPackId(scope.package, scope.packageVersion);
                        });
                    } else if (scope.packageVersion && scope.package){
                        scope.packId = getPackId(scope.package, scope.packageVersion);
                    } else if (!scope.package) {
                        throw "Error in packageLink directive parameters, expected either packageVersionId or packageVersion and package";
                    } else {
                        scope.packId = getPackId(scope.package);
                    }
                }
            };
        }
    ]);
