'use strict';

/**
 * @ngdoc directive
 * @name biowebEvolApp.directive:TopicBadge
 * @description displays an EDAM topic as a badge with its description
 * as a caption and linking to the list of related packages.
 * # TopicBadge
 */
angular.module('biowebEvolApp')
    .directive('topicBadge', ['Edam',
        function(Edam) {
            return {
                templateUrl: '/scripts/directives/topicbadge.html',
                scope: {
                    topicId: '=topicId',
                },
                restrict: 'E',
                link: function(scope) {
                    Edam.get(scope.topicId).promise.then(function(data) {
                        scope.edamTerm = data;
                    });
                }
            };
        }
    ]);