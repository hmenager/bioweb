angular.module('biowebEvolApp')
    .directive('newsList', ['news',
        function(news) {
            return {
                templateUrl: '/scripts/directives/newslist.html',
                scope: {
                    limit: '@'
                },
                restrict: 'E',
                link: function(scope) {
                  scope.news = news.query({limit:scope.limit});
                }
            };
        }
    ]);
