'use strict';

angular.module('biowebEvolApp')
    .controller('MessageCtrl', function($rootScope, $scope, $window, $location) {
      $rootScope.keepMessage=false;
      $rootScope.$on("$routeChangeError", function(event, current, previous, rejection){
        $rootScope.message = rejection;
        $rootScope.keepMessage=true;
        if (previous) {
          $window.history.back();
        } else {
          $location.path("/").replace();
        }
      });
      $rootScope.$on("$routeChangeSuccess", function(){
        if($rootScope.keepMessage==false){
          $rootScope.message = null;
        }
        $rootScope.keepMessage=false;
      });
    });
