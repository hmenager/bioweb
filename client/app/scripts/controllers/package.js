'use strict';

angular.module('biowebEvolApp')
  .controller('PackageCtrl', function($scope, $routeParams, $rootScope, pack) {
    if(pack.type==="package"){
      $scope.package = pack;
      $rootScope.title = $scope.package.name;
    }else{
      $scope.packageVersion = pack;
      $scope.package = pack.package;
      $rootScope.title = $scope.package.name + ' ' + $scope.packageVersion.version;
    }
  });
