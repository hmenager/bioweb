'use strict';

angular.module('biowebEvolApp')
    .controller('DatabanksCtrl', function($scope, Catalog, Edam, $rootScope, $routeParams, $location) {
        $rootScope.title = 'Databanks';
        $scope.routeParams = $routeParams;
        var load = function(dataPromise){
          $scope.packages=[];
          $scope.loading=true;
          dataPromise.then(function(data){
            $scope.searchSources = [];
            $scope.banks = data;
            $scope.loading=false;
          });
        }
        $scope.entriesShown = 10;
        $scope.loadMore = function(){
          $scope.entriesShown += 10;
        }
        load(Catalog.banks($routeParams.search).promise);
        $scope.$watch('routeParams', function(newVal, oldVal) {
            if(newVal==oldVal){
              return;
            }
            angular.forEach(newVal, function(v, k) {
              if (k != 'topicId') {
                if(v!=null){
                  v = v.trim(); //remove useless whitespaces
                  if(v===''){
                      v=null; //setting a value to null will unset the parameter in the URL
                  }
                }
                $location.search(k, v);
              }
            });
            load(Catalog.banks($routeParams.search).promise);
        }, true);
    });
