'use strict';

angular.module('biowebEvolApp')
    .controller('PackagesListCtrl', function($scope, Catalog, Edam, $routeParams, $rootScope, $q, webOnly, collection, $location, $timeout) {
        $rootScope.title = 'Tools and packages';
        $scope.routeParams = $routeParams;
        $scope.hasMobyleInterfaces = function(version) {
            return _.some(version.programs, function(program) {
                return program.mobyles.length > 0;
            });
        };
        $scope.hasGalaxyInterfaces = function(version) {
            return _.some(version.programs, function(program) {
                return program.galaxys.length > 0;
            });
        };
        $scope.entriesShown = 10;
        $scope.loadMore = function(){
          $scope.entriesShown += 10;
        }
        $scope.$watch(function(){
         // detect any size change in the table to see if loadMore() must be called
         try{
           return ($('#packagesTable').height() || 0) + ($('#packagesTable').offset().top || 0);
         }catch(e){
           return 0;
         }
        },function onHeightChange(newValue, oldValue){
          if(window.innerHeight > newValue){
            $timeout($scope.loadMore);
          }
        });
        $q.all([Catalog.packageVersions(null, webOnly, collection).promise, Edam.topicsList.promise]).then(function(results){
          $scope.searchSources = _.flatten(results);
        });
        if (webOnly) {
            $scope.hasWebInterfacesFilter = function(item) {
                return _.some(item.versions, function(version) {
                    return _.some(version.programs, function(program) {
                        return (program.galaxys.length + program.mobyles.length) > 0;
                    });
                });
            };
            $scope.versionHasWebInterfacesFilter = function(version) {
                return _.some(version.programs, function(program) {
                    return (program.galaxys.length + program.mobyles.length) > 0;
                });
            };
            $rootScope.title = 'Web ' + $rootScope.title;
        }
        var loadPackages = function(dataPromise){
          $scope.packages=[];
          $scope.loading=true;
          dataPromise.then(function(packages){
            $scope.packages = packages;
            $scope.entriesShown = 0;
            $scope.loadMore();
            $scope.loading=false;
          });
        }
        $scope.$watch('routeParams', function(newVal, oldVal) {
            if(newVal==oldVal){
              return;
            }
            angular.forEach(newVal, function(v, k) {
              if (k != 'topicId') {
                if(v!=null){
                  v = v.trim(); //remove useless whitespaces
                  if(v===''){
                      v=null; //setting a value to null will unset the parameter in the URL
                  }
                }
                $location.search(k, v);
              }
            });
            loadPackages(Catalog.searchPackages($routeParams.search, $routeParams.topicId, webOnly, null, collection).promise);
        }, true);
        loadPackages(Catalog.searchPackages($routeParams.search, $routeParams.topicId, webOnly, null, collection).promise);
        if ($routeParams.topicId) {
            Edam.topics().promise.then(function(edamTerms){
              $rootScope.title += ' by theme > ' + edamTerms[$routeParams.topicId]['rdfs:label'];
            });
        }
        $scope.query = $routeParams.search;
        $scope.edamTerms = Edam.topics();
    });
