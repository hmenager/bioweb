'use strict';

angular.module('biowebEvolApp')
    .controller('MainCtrl', ['$scope', 'Catalog', '$routeParams', '$location', '$rootScope', 'Edam', '$q',
     function($scope, Catalog, $routeParams, $location, $rootScope, Edam, $q) {
        $rootScope.title = 'Welcome!';
        $scope.routeParams = $routeParams;
        $scope.searchSources = [];
        // this loop precomputes the span of the different sections
        for (var i = 0; i < $scope.sections.length; i++) {
          if($scope.sections.length%2===1 && i>=$scope.sections.length-3){
              $scope.sections[i].colSpan = 4;
          }else{
              $scope.sections[i].colSpan = 6;
          }
        }
        $scope.matches = {};
        $scope.loading = {};
        var load = function(){
          _.map($scope.sections, function(section){
            $scope.matches[section.name] = null;
            $scope.loading[section.name] = true;
            section.loader().promise.then(function(matchesNumber){
              $scope.matches[section.name] = matchesNumber;
              $scope.loading[section.name] = false;
            });
          });
        };
        $scope.$watch('routeParams', function(newVal, oldVal) {
            if(newVal==oldVal){
              return;
            }
            angular.forEach(newVal, function(v, k) {
              if (k != 'topicId') {
                if(v!=null){
                  v = v.trim(); //remove useless whitespaces
                  if(v===''){
                      v=null; //setting a value to null will unset the parameter in the URL
                  }
                }
                $location.search(k, v);
              }
            });
            load();
        }, true);
        load();
    }]);
