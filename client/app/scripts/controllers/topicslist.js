'use strict';

angular.module('biowebEvolApp')
    .controller('TopicsListCtrl', function($scope, Catalog, Edam, $rootScope, $q, $routeParams, $location) {
        $rootScope.title = 'Topics';
        $scope.routeParams = $routeParams;
        Edam.topics().promise.then(function(edamTerms){
          var load = function(dataPromise){
            $scope.packages=[];
            $scope.loading=true;
            dataPromise.then(function(topicIds){
              $scope.searchSources = [];
              $scope.topicTags = _.map(topicIds, function(topicCount) {
                  try {
                      return {
                          'text': edamTerms[topicCount._id]['rdfs:label'],
                          'link': '/packages/topics/' + edamTerms[topicCount._id].shortId,
                          'weight': topicCount.count,
                          'html': {
                              'title': edamTerms[topicCount._id]['oboInOwl:hasDefinition']
                          }
                      };
                  } catch (e) {
                      console.trace(e);
                  }
              });
              $scope.loading=false;
            });
          };
          load(Catalog.packageTopics($routeParams.search).promise);
          $scope.$watch('routeParams', function(newVal, oldVal) {
              if(newVal==oldVal){
                return;
              }
              angular.forEach(newVal, function(v, k) {
                if (k != 'topicId') {
                  if(v!=null){
                    v = v.trim(); //remove useless whitespaces
                    if(v===''){
                        v=null; //setting a value to null will unset the parameter in the URL
                    }
                  }
                  $location.search(k, v);
                }
              });
              load(Catalog.packageTopics($routeParams.search).promise);
          }, true);
        });
    });
