'use strict';

angular.module('biowebEvolApp')
    .service('news', function Edam($resource) {
        return $resource('/api/news');
    });
