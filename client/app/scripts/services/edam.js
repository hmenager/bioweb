'use strict';

angular.module('biowebEvolApp')
    .service('Edam', function Edam($http, $q) {
        var topicsMap = $q.defer();
        var topicsList = $q.defer();
        $http({
            method: 'GET',
            url: '/api/edam/subsets/topics'
        }).success(function(data) {
            var map = {};
            topicsList.resolve(data);
            _.map(data, function(topic) {
                topic.shortId = topic._id.split('/').pop();
                map[topic._id] = topic;
                map[topic.shortId] = topic;
            });
            topicsMap.resolve(map);
        });
        var topics = function(ids) {
            if (!ids) {
                return topicsMap;
            } else {
                var res = $q.defer();
                topicsMap.promise.then(function(data) {
                    var map = {};
                    _.map(ids, function(id) {
                        map[id] = data[id];
                    });
                    res.resolve(map);
                });
                return res;
            }
        };
        var get = function(id) {
            var res = $q.defer();
            topicsMap.promise.then(function(data) {
                res.resolve(data[id]);
            });
            return res;
        };
        return {
            topics: topics,
            get: get,
            topicsList: topicsList
        };
    });
