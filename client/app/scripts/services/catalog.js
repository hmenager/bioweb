'use strict';

angular.module('biowebEvolApp')
    .service('Catalog', function Catalog($http, $q) {

        var banks = function(query, count){
          var searchResults = $q.defer();
          query = query || '';
          count = count || '';
          $http({
              method: 'GET',
              url: '/api/data?search=' + query + '&count=' + count
          }).success(function(data, status) {
              searchResults.resolve(data);
          });
          return searchResults;
        }

        var packageVersions = function(packageId, onlyWeb, collection){
          var result = $q.defer();
          var url = '/api/packageVersions';
          if(packageId!=null){
            url += '/'+packageId;
          }
          var params = {
            'web': onlyWeb,
            'collection': collection
          }
          $http({
            method: 'GET',
            url: url,
            params: params
          }).success(function(data, status){
            result.resolve(data);
          });
          return result;
        }

        var packageTopics = function(query, count, collection){
            var searchResults = $q.defer();
            query = query || '';
            count = count || '';
            var params = {
              search: query,
              count: count,
              collection: collection
            }
            $http({
                method: 'GET',
                url: '/api/packageTopics',
                params: params
            }).success(function(data, status) {
                searchResults.resolve(data);
            });
            return searchResults;
        };

        var searchPackages = function(query, topicId, web, count, collection) {
            var searchResults = $q.defer();
            var params = {
              search: query,
              count: count,
              topicId: topicId,
              collection: collection,
              web: web
            }
            $http({
                method: 'GET',
                url: '/api/packages',
                params: params
            }).success(function(data, status) {
              _.map(data, function(item){
                _.map(item.versions, function(packVersion) {
                    packVersion.getPackage = function() {
                        return item;
                    }
                });
              });
              searchResults.resolve(data);
            });
            return searchResults;
        };

        return {
            'searchPackages': searchPackages,
            'packageTopics': packageTopics,
            'packageVersions': packageVersions,
            'banks': banks,
        };

    });
