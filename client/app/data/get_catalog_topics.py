#!/usr/bin/env python
import json
edam = json.load(open('edam.json'))
catalog = json.load(open('catalog.json'))
catalog_new = []
for i in catalog:
    topics = []
    for ot in i.get('categories',[]):
        matches = [term for term in edam if term['@id']=='http://edamontology.org/topic_'+ot[-4:]]
        if matches:
            #print i['name'], matches
            topics.append(matches[0]['@id'])
    i['topics'] = topics
    catalog_new.append(i)
json.dump(catalog_new, open('catalog_new.json','w'), indent=2, sort_keys=True)
