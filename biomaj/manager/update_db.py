import argparse
import pymongo
import json

def process_file(file_path):
    data = json.load(open(file_path))
    client = pymongo.MongoClient('localhost')
    catalog = client['bioweb']['catalog']
    print catalog.remove({'type':'bank'})
    inserted_banks = 0
    for bank_versions in data['banks']:
        for bank_version in bank_versions:
            catalog.insert(bank_version)
            inserted_banks += 1
    print inserted_banks

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                 description='BioMAJ JSON import tool')
    parser.add_argument('file',
                        help="path to the BioMAJ JSON file")
    args = parser.parse_args()

    process_file(args.file)
