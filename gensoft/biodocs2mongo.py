#! /local/gensoft2/adm/bin/python

from __future__ import print_function

import argparse
import ConfigParser
import os
import pymongo
import re
import ssl
import string
import sys


from pprint import pprint

#### TODO !!!!!
# 
# add config entry for use module.
# if set => check program listing from biodocs versus program listing from modules



#---- import homebrew modules

PYMODULEDIR="/local/gensoft2/adm/share/gensoft/pymodules"
sys.path.insert(0, PYMODULEDIR)
import BiodocParser

#---- some hugly global variables

DAT='.'
ERRFH = sys.stderr
LOGFH = sys.stdout
FATAL  = 1
WARN   = 0

def err(exit_val, *msg):
    head=['Warning', 'Error']
    print ("%s: %s" % (head[exit_val], " - ".join(map(str, msg))), file=ERRFH)
    if exit_val:
        sys.exit(exit_val)
    return None

def log(*msg):
    if VERBOSE:
        print ("%s" %(' '.join(map(str, msg))), file=LOGFH)
        LOGFH.flush()


def _id_generator(*arg):
    return ID_SEPARATOR.join(arg)

def check_missing_biodocs(biodocs_lst):
    ret = []
    for biodoc in biodocs_lst:
        if not os.path.isfile(biodoc):
            err(WARN, biodoc, 'no such file')
            continue
        ret.append(biodoc)
    return ret

def get_biodocs(packdir):
    # filter to keep only pack_version 
    pack_name = os.path.basename(packdir)
    pack_version_lst = [ os.path.join(packdir, d) for d in os.listdir(packdir) if d.startswith(pack_name) ]
    biodocs_lst = [ os.path.join(d, 'BIODOCS.yaml') for d in pack_version_lst ]
    return check_missing_biodocs(biodocs_lst)

def citation_key_translate(refs_col):
    # mapping = {key in yaml : key expected in mongo  }
    mapping = {'ID': 'ID'
              ,'idType' : 'IDtype'
              ,'title': 'citation'
              }
    for ref in refs_col:
        for k, v in mapping.items():
            ref[v] = ref.pop(k)
    return refs_col


def info2package(info):
    pack_name =  info['name']
    pack_id = _id_generator('pack', pack_name)
    pack_doc = {
         '_id': pack_id
       , 'type' : 'package'
       , 'name' : info['name']
       , 'description' : info['description']
       , 'home' : info['home']
       , 'source' : info['sources']
       , 'authors' : info['authors']
       , 'categories' : info['operations'] + info['topics']
       , 'topics' : info['topics']
       , 'operations': info['operations']
       , 'collections' : [info['origin']]
       , 'history' : info['history']
       , 'library' : info['library']
       , 'private' : info['private']
       , 'references' : citation_key_translate(info['references'])
    }
    return pack_doc
   

def info2pack_version(info): 
    pack_name =  info['name']
    pack_id = _id_generator("pack", pack_name)
    pack_version = info['version']
    pack_version_id = _id_generator("pack", pack_name, pack_version)
    pack_version_doc = {
        '_id' : pack_version_id
      , 'type' : 'packageVersion'
      , 'package' : pack_id
      , 'default' : info['default']
      , 'version' : info['version']
      , 'depends' : info['depends']
      , 'doc'     : {'docs' : []
                   , 'html' : info['htmldocs']
                   , 'man'  : []
                   }
      }
    return pack_version_doc


def info2progs(info):
    progs = []
    pack_name =  info['name']
    pack_version = info['version']

    if not 'programs' in info:
        if not info['library']:
            err(FATAL, '%s/%s: no program documented' % (pack_name, pack_version))
        else:
            do_document = []
    else:
        do_document = info['programs']
    for elem in do_document:
        new = {}
        new['_id'] = _id_generator("prog", pack_name, pack_version, elem['name'])
        new['type'] = 'program'
        new['name'] = elem['name']
        new['description'] = elem['description'] if elem['description'] else ''
        new['packageVersion'] = _id_generator("pack", pack_name, pack_version)
        new['categories'] = []
        new['doc'] = {'html': [], 'man': [elem['manpages']]}
        progs.append(new)
    return progs


def info2mobyle(info):
    # import mobyledefs
    ##---- should no longer used with Biodocs.yaml
    # is the version used by mobyle ?
    # if "%s/%s" %(pack_name, pack_version) not in mobyledefs.pack_info:
    #     return mobyle

    mobyle = []
    pack_name =  info['name']
    pack_version = info['version']
    pattern= '(?P<name>[\w-]+)\s*\[(?P<desc>.*)\]'
    auto = re.compile(pattern)
    # we already checked that packholds programs or not
    to_document = info.get('programs', [])
    for prg in to_document:
        prog_name = prg['name']
        # something to document ?
        web = prg.get('web', False)
        if not web:
            continue
        for web_info in web:
            interface = {}    
            match = auto.match(web_info)
            if match is None:
                err(WARN, 'invalib web description', web_info)
            i_name = match.group('name')
            i_desc = match.group('desc')
            interface['_id'] = _id_generator("mobyle", pack_name, pack_version, i_name)
            interface['type'] = 'mobyle'
            interface['package'] = _id_generator("pack", pack_name, pack_version)
            interface['programs'] = [_id_generator("prog", pack_name, pack_version, prog_name)]
            interface['name'] = i_name
            interface['description'] = i_desc
            interface['url'] = "%s#forms::%s" % (MOBYLEURL, interface['name'])
            mobyle.append(interface)
    return mobyle 


def mongo_connect(host, port, db, col):
    log('connect to', host, 'on port', port)

    try:
        client = pymongo.MongoClient(host, port, j=True, ssl=True, ssl_cert_reqs=ssl.CERT_NONE)
    except pymongo.errors.ConnectionFailure as msg:
        err(FATAL, "mongodb %s/%s" %(host, port), msg)
    db = client[db]
    col = db[col]
    return col


def escapePackName(name):
    ret = ''
    for c in name:
        if c in string.punctuation:
            ret += '\\'
        ret += c
    return ret


def purge(col, pack_id, pack_version_id):
    '''
    remove any previous package related documents
    not the fatest but the safest.
    '''
    # remove package info
    ret = col.remove({'type': 'package', '_id': pack_id}) 
    # remove packageVersion info
    ret = col.remove({'type': 'packageVersion', '_id': pack_version_id}) 
    # remove program info 
    ret = col.remove({'type': 'program', 'packageVersion': pack_version_id})
    # remove mobyle infos
    ret = col.remove({'type': 'mobyle', 'package': pack_version_id})

def insert(col, pack_name, pack_version, *args):
    entries = []
    for e in args:
        e = e if isinstance(e, list) else [e]
        entries.extend(e)
        
    print("\tinsert %s %s" %( pack_name, pack_version), end='')
    n = 0
    try:
        n = col.insert(entries)
    except pymongo.errors.DuplicateKeyError as msg:
        err(WARN, "unable to insert from", pack_name, pack_version , msg)
    except pymongo.errors as msg:
        err(WARN, msg)
    msg = 'OK' if n else "Failed"
    print("::\t%s" %(msg))
        

if __name__ == '__main__':

    #---- default values
    config_file = os.path.join(DAT, 'config.cfg')
    cfg = ConfigParser.SafeConfigParser()
    if not cfg.read(config_file):
        err(FATAL, 'No configuration file found')
  
    ID_SEPARATOR = cfg.get('DEFAULT', 'ID_SEPARATOR') 
    VERBOSE = cfg.get('DEFAULT', 'VERBOSE') 
    HOST = cfg.get('MONGO', 'HOST')
    PORT = int(cfg.get('MONGO', 'PORT'))
    DB = cfg.get('MONGO', 'DB')
    COL = cfg.get('MONGO', 'COL')
    JOURNALING = cfg.get('MONGO', 'JOURNALING')
    MOBYLEURL = cfg.get('MOBYLE', 'MOBYLEURL')
    
    #---- get command line
    parser = argparse.ArgumentParser(description='Gensoft to bioweb mongo db conversion tool.'
                                    ,epilog="example: biodocs2mongo.py /local/gensoft2/inst/toppred" )
    parser.add_argument('--host', action="store"
                        , type=str
                        , default=HOST
                        , dest='db_host',
                        help='mongo server host.')
    parser.add_argument('--port',action='store'
                        , type=int
                        , default=PORT
                        , dest='db_port',
                        help='mongo server port.')
    parser.add_argument('--test', action='store_true'
                       , default = False
                       , dest='test'
                       , help='perform the job but does not insert on DB')
    parser.add_argument('--db'
                        ,action='store'
                        , type=str
                        , default=DB
                        , dest='db_name',
                        help='mongo DB to work with.')
    parser.add_argument('-v'
                        , action='store_true'
                        , default=VERBOSE
                        , dest='VERBOSE', \
                        help='Turns verbosity on.')
    parser.add_argument('args', nargs='*', help='inst dir of package to document')
    cmdline = parser.parse_args()
   
    #---- update config from cmdline options.
    HOST=cmdline.db_host
    PORT=cmdline.db_port
    VERBOSE=cmdline.VERBOSE


    #---- get mongo connection.
    col =  mongo_connect(HOST, PORT, DB, COL)

    for packdir in cmdline.args:
        packdir = os.path.abspath(packdir)
        log(packdir)
        for biodoc in get_biodocs(packdir):
            info = BiodocParser.get_packversion(biodoc)
            pack_name =  info['name']
            pack_id = _id_generator("pack", pack_name)
            pack_version = info['version'] 
            pack_version_id = _id_generator("pack", pack_name, pack_version)
    
            pack_doc = info2package(info)
            pack_version_doc = info2pack_version(info)
            program_docs = info2progs(info)
            mobyle_docs = info2mobyle(info)
            if cmdline.test:
                print(">>>>>>>>>>>>>> pack")
                pprint(pack_doc)
                print(">>>>>>>>>>>>>> packversion")
                pprint(pack_version_doc)
                print(">>>>>>>>>>>>>> programs")
                pprint(program_docs)
                print(">>>>>>>>>>>>>> mobyle")
                pprint(mobyle_docs)
                continue

            purge(col, pack_id, pack_version_id)
            insert(col, pack_name, pack_version,  pack_doc, pack_version_doc, program_docs, mobyle_docs)
    
